import ntplib
import time
import math
c = ntplib.NTPClient()
response = c.request("north-america.pool.ntp.org", version=3)

# Negative offset = future = clock is fast
# Positive offset =  past  = clock is slow
# Offset is given in seconds
print("Offset:", response.offset)

display = True
ltime = math.floor(time.time() + response.offset)

while True:
	ctime = time.time() + response.offset
	
	if ctime - ltime >= 1:
		display = True
	
	if display:
		ltime = math.floor(ctime)
		print("Real time:", ctime)
		display = False
		
	time.sleep(0.001)

'''
print(response.offset)
print(response.version)
#from time import ctime
print(ctime(response.tx_time))
print(ntplib.leap_to_text(response.leap))
print(response.root_delay)
print(ntplib.ref_id_to_text(response.ref_id))
'''